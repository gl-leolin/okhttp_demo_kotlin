package com.example.okhttp_demo_kotlin

import PIACertPinningAPI
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import okhttp3.*
import java.io.IOException
import java.io.InputStream
import java.util.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var okBtn = findViewById<Button>(R.id.myBtn);
        okBtn.setOnClickListener() {
            sendMessage();
        }
    }

    fun sendMessage() {
        Log.e("Leo", "sendMessage");
        var host = "156.146.34.29";
        var port = 1337;
        var wgPubKey = "+Ii7iyDSS7Pr+8uu6Z4cCY9UXPWp0mKUyvY9DavuPX0=";
        var authToken = "2rhUBuhjfMX9q5grZLlynbApGv_84Iyd3HxJZ5mhHtwFqmhzRTsXtvvghVpl1bWsEgA_53UisCINPxZkuReICkQcOzkYL4YWGTM2APxwHggcActOtKd3T6+C6C0=";
        var commonName = "tokyo403"

        val httpUrl: HttpUrl = HttpUrl.Builder()
            .scheme("https")
            .host(host)
            .port(port)
            .addPathSegment("addKey")
            .addQueryParameter("pubkey", wgPubKey)
            .addQueryParameter("pt", authToken)
            .build()

        val request = Request.Builder().url(httpUrl).build()
        val piaApi = PIACertPinningAPI(this.applicationContext)

        // Set the endpoints/cn for the selected protocol before the request
        val endpointCommonNames: MutableList<Pair<String, String>> = mutableListOf()
        var pair = Pair(host, commonName);
        endpointCommonNames.add(pair);
        piaApi.setKnownEndpointCommonName(endpointCommonNames);

        val client: OkHttpClient = piaApi.okHttpClient;
        Log.d("Wireguard", "httpUrl: $httpUrl")
        Log.d("Wireguard", "Attempting call")

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (!response.isSuccessful) throw IOException("Unexpected code $response")

                    for ((name, value) in response.headers) {
                        println("$name: $value")
                    }

                    println(response.body!!.string())
                }
            }
        })
    }

    fun getRSA4096Certificate(): InputStream? {
        return try {
            assets.open("rsa4096.pem")
        } catch (e: IOException) {
            throw IllegalStateException("Failed to load the RSA4096 certificate. $e")
        }
    }
}